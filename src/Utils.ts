import DomNode from './DomNode';

class Utils {

	public static isSubtree(dom: DomNode, vdom: DomNode): boolean {
		return Utils.stringFromPreOrder(dom).indexOf(Utils.stringFromPreOrder(vdom)) > -1;
	}

	public static stringFromPreOrder(tree: DomNode): string {
		if (!tree) {
			return '';
		}

		return tree.value + Utils.stringFromPreOrder(tree.left) + Utils.stringFromPreOrder(tree.right);
	}

}

export default Utils;
