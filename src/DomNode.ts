interface IDomNode {
	value: string;
	left?: IDomNode;
	right?: IDomNode;
}

export default IDomNode;
