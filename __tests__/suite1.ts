import DomNode from '../src/DomNode';
import Utils from '../src/Utils';

/**
 * Test to validate the provided assignment.
 * Serves as a starting point with the code provided.
 */
test('validate assignment', () => {

	const dom: DomNode  = {
		value: 'root',
		left: {
			value: 'a',
			left: {
				value: 'c',
				left: {
					value:  'g'
				},
				right: {
					value: 'h'
				}
			},
			right: {
				value: 'd',
				left: {
					value: 'i'
				}
			}
		},
		right: {
			value: 'b',
			left: {
				value: 'e',
				right: {
					value: 'j',
					left: {
						value: 'k'
					},
					right: {
						value: 'l'
					}
				}
			},
			right: {
				value: 'f'
			}
		}
	};

	const vdom: DomNode = {
		value: 'a',
		left: {
			value: 'c',
			left: {
				value: 'g'
			},
			right: {
				value: 'h'
			}
		},
		right: {
			value: 'd',
			left: {
				value: 'i'
			}
		}
	};

	expect(Utils.isSubtree(dom, vdom)).toBe(true);
});
