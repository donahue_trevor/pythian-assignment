### Pythian coding challenge

The questions and answers are organized in branches

| Question # | Branch | Link |
|------------|--------|------|
| Question 1 | q1     | [click](https://bitbucket.org/donahue_trevor/pythian-assignment/src/d3149e3e4b962131dcd723006ef43f1295ee9d17/?at=q1) |
| Question 2 | q2     | [click](https://bitbucket.org/donahue_trevor/pythian-assignment/src/1511152dd802487e0ab1ba0fadcc30debbd347e3/?at=q2) |
| Question 3 | q3     | [click](https://bitbucket.org/donahue_trevor/pythian-assignment/src/e555992f5d4ad23a9946f99c7de11cecae4f418b/?at=q3) |
| Question 4 | q4     | [click](https://bitbucket.org/donahue_trevor/pythian-assignment/src/222cbad3e738174bc711443e7ef6929b70c9b664/?at=q4) |
| Question 5 | q5     | [click](https://bitbucket.org/donahue_trevor/pythian-assignment/src/d7686b90e521b6c170c61fd83d60d50ddea83c95/?at=q5) |

Each branch has a detailed README.md file describing the answer for each question.

I hope you have enjoyed reading this as much as I enjoyed writing it.

Thank you,
Alex Gherman
